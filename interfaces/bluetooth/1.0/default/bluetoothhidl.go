// Copyright (C) 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package bluetoothhidl

import (
	"android/soong/android"
	"android/soong/cc"
)

func init() {
	android.RegisterModuleType(
		"bluetoothhidl_defaults",
		func() android.Module {
			module := cc.DefaultsFactory()
			android.AddLoadHook(module, bluetoothhidlLoadHook)
			return module
		})
}

func bluetoothhidlLoadHook(ctx android.LoadHookContext) {
	type props struct {
		Cflags       []string
		Static_libs  []string
		Include_dirs []string
	}

	p := &props{}
	var board_bluetooth_vendor string
	board_bluetooth_vendor = ctx.Config().VendorConfig("vendor").String("board_bluetooth_vendor")
	if len(board_bluetooth_vendor) > 0 && string(board_bluetooth_vendor) == "common" {
		p.Static_libs  = append(p.Static_libs, "libhwinfo")
		p.Include_dirs = append(p.Include_dirs, "hardware/aw/wireless/hwinfo")
		p.Cflags       = append(p.Cflags, "-DBOARD_BLUETOOTH_VENDOR_COMMON")
		p.Cflags       = append(p.Cflags, "-include libhwinfo.h")
	}

	ctx.AppendProperties(p)
}
