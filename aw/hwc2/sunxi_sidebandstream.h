
#ifndef _sunxi_sidebandstream_
#define _sunxi_sidebandstream_

#include <utils/NativeHandle.h>

typedef struct sunxi_sideband_handle {
    native_handle_t base;
    uint32_t token;
    uint32_t flags;
    uint32_t source;
} sunxi_sideband_handle_t;

#define SUNXI_SIDEBAND_HANDLE_NUM_INT (3)
#define SUNXI_SIDEBAND_HANDLE_NUM_FD  (0)
#define SUNXI_SIDEBAND_IDENTIFIER     (0x00157200)

inline native_handle_t* create_sideband_handle() {
    sunxi_sideband_handle_t* handle = (sunxi_sideband_handle_t*)
        native_handle_create(SUNXI_SIDEBAND_HANDLE_NUM_FD, SUNXI_SIDEBAND_HANDLE_NUM_INT);
    handle->token  = SUNXI_SIDEBAND_IDENTIFIER;
    handle->flags  = 0;
    handle->source = 0;
    return (native_handle_t *)handle;
}

inline bool validated_sunxi_sidebandstream(const native_handle_t* stream) {
    if (stream && stream->numFds == SUNXI_SIDEBAND_HANDLE_NUM_FD &&
            stream->numInts == SUNXI_SIDEBAND_HANDLE_NUM_INT) {
        const sunxi_sideband_handle_t* handle = (const sunxi_sideband_handle_t*)(stream);
        return handle->token == SUNXI_SIDEBAND_IDENTIFIER;
    }
    return false;
}

#endif
