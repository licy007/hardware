
#ifndef __HAL_CAMERA_MANAGER_H__
#define __HAL_CAMERA_MANAGER_H__

#include "CameraDebug.h"
#include <fcntl.h>
#include <cutils/list.h>
#include <utils/Mutex.h>
#include <type_camera.h>
#include "CameraHardware2.h"

#ifdef CAMERA_MANAGER_ENABLE

#define NB_COMPOSE_BUFFER 6
namespace android {
typedef struct BufManager_t
{
    int write_id;
    int read_id;
    int buf_used;
    pthread_mutex_t mutex;
    pthread_cond_t condition;
    V4L2BUF_t   *buf[NB_COMPOSE_BUFFER]; //use V4L2BUF_t * just to do not save buffer again
}BufManager;

enum CaptureState {
        CAPTURE_STATE_PAUSE,
        CAPTURE_STATE_READY,
        CAPTURE_STATE_STARTED,
        CAPTURE_STATE_EXIT,
        CAPTURE_STATE_IDLE,
};

class CameraManager {
public:
    CameraManager();
    ~CameraManager();
    bool composeThread();
    bool commandThread();
    status_t setCameraHardware(int index,CameraHardware *hardware);
    int setFrameSize(int index,int width,int height);
    int setCaptureState(int index,int state);
    int queueCameraBuf(int index,V4L2BUF_t *buffer);
    void startPreview();
    void stopPreview();
    void releaseCamera();
    inline void setOviewEnable(bool enable)
    {
        mIsOview= enable;
    }
    inline bool isOviewEnable(void)
    {
        return mIsOview;
    }

protected:
    typedef enum CMD_CM_QUEUE_t{
        CMD_CM_START_PREVIEW    = 0,
        CMD_CM_STOP_PREVIEW,
        CMD_CM_RELEASE_CAMERA,
        CMD_CM_QUEUE_MAX
    }CMD_CM_QUEUE;
    OSAL_QUEUE  mQueueCMCommand;
    typedef struct Queue_CM_Element_t {
        CMD_CM_QUEUE cmd;
        int data;
    }Queue_CM_Element;
    Queue_CM_Element mQueueCMElement[CMD_CM_QUEUE_MAX];
    class DoCommandThread : public Thread {
        CameraManager* mCameraManager;
        bool                mRequestExit;
    public:
       DoCommandThread(CameraManager* dev) :
                Thread(false),
                mCameraManager(dev),
                mRequestExit(false) {
            }
            void startThread() {
                run("CMDoCommandThread", PRIORITY_URGENT_DISPLAY);
            }
            void stopThread() {
                mRequestExit = true;
            }
            virtual bool threadLoop() {
                if (mRequestExit) {
                    return false;
                }
                return mCameraManager->commandThread();
            }
    };
    sp<DoCommandThread> mCommandThread;
    pthread_mutex_t mCommandMutex;
    pthread_cond_t mCommandCond;
        class ComposeThread : public Thread {
            CameraManager* mCameraManager;
            bool mRequestExit;
        public:
            ComposeThread(CameraManager* dev) :
                Thread(false),
                mCameraManager(dev),
                mRequestExit(false) {
            }
            void startThread() {
                run("ComposeThread", PRIORITY_URGENT_DISPLAY);
            }
            void stopThread() {
                mRequestExit = true;
            }
            virtual bool threadLoop() {
                if (mRequestExit) {
                    return false;
                }
                return mCameraManager->composeThread();
            }
        };
public:
    int mStartCameraID;
    int mCameraTotalNum;
    int mFrameWidth;
    int mFrameHeight;
    void releaseByIndex(int index);
private:
    CaptureState    mCaptureState;
    bool mIsOview;
    bool mCameraState[MAX_NUM_OF_CAMERAS];
    int mReleaseIndex[NB_BUFFER][MAX_NUM_OF_CAMERAS];
    BufManager mCameraBuf[MAX_NUM_OF_CAMERAS];
    int mAbandonFrameCnt;
    CameraHardware *mCameraHardware[MAX_NUM_OF_CAMERAS];
    pthread_mutex_t mComposeMutex;
    pthread_cond_t  mComposeCond;
    sp<ComposeThread> mComposeThread;
    bool canCompose();
    bool isSameSize();
    void releaseAllCameraBuff();
};

}; /* namespace android */

#endif //CAMERA_MANAGER_ENABLE

#endif  /* __HAL_CAMERA_MANAGER_H__ */


