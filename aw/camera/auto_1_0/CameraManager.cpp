#include "CameraDebug.h"

#if DBG_CAMERA_MANAGER
#define LOG_NDEBUG 0
#endif
#define LOG_TAG "CameraManager"
#include <cutils/log.h>
#include "CameraManager.h"
#include <sys/time.h>

#ifdef CAMERA_MANAGER_ENABLE
#define SAVE_FRAME 0
#if SAVE_FRAME
static int count = 0;
#endif
#define ABANDONFRAMECT  15
#define MAX_WIDTH 1280
#define MAX_HEIGHT 720
namespace android {

CameraManager::CameraManager()
    :mStartCameraID(CAMERA_ID_START)
    ,mCameraTotalNum(NB_CAMERA)
    ,mFrameWidth(MAX_WIDTH)
    ,mFrameHeight(MAX_HEIGHT)
    ,mCaptureState(CAPTURE_STATE_PAUSE)
    ,mIsOview(false)
    ,mAbandonFrameCnt(ABANDONFRAMECT)
{
    memset(&mCameraBuf,0x0,sizeof(BufManager)*MAX_NUM_OF_CAMERAS);
    memset(mCameraState,0,sizeof(bool)*MAX_NUM_OF_CAMERAS);
    memset(mReleaseIndex, 0, sizeof(mReleaseIndex));
    for(int i=0;i<MAX_NUM_OF_CAMERAS;i++)
    {
        mCameraHardware[i] = NULL;
    }

    pthread_mutex_init(&mComposeMutex,NULL);
    pthread_cond_init(&mComposeCond,NULL);

    // init command queue
    OSAL_QueueCreate(&mQueueCMCommand, CMD_CM_QUEUE_MAX);
    memset((void*)mQueueCMElement, 0, sizeof(mQueueCMElement));

    // init command thread
    pthread_mutex_init(&mCommandMutex, NULL);
    pthread_cond_init(&mCommandCond, NULL);
    mCommandThread = new DoCommandThread(this);
    mCommandThread->startThread();

    // init Compose thread
    mComposeThread = new ComposeThread(this);
    mComposeThread->startThread();
    F_LOGD;

}

bool CameraManager::canCompose()
{
    int i,j;
    bool canCompose = true;

    pthread_mutex_lock(&mComposeMutex);

    for (i = 0; i < mCameraTotalNum; i++)
    {
        if (mCameraBuf[i].buf_used <= 0)
        {
            ALOGV("mCameraBuf[camera %d].buf_used==0", i + mStartCameraID);
            canCompose = false;
            break;
        }
    }

    for (i = 0; i < mCameraTotalNum; i++)
    {
        if (mCameraBuf[i].buf_used > NB_COMPOSE_BUFFER -2)
        {
            V4L2BUF_t *buffer;
            //memcpy(&buffer, &mCameraBuf[i].buf[mCameraBuf[i].read_id], sizeof(V4L2BUF_t));
            buffer = mCameraBuf[i].buf[mCameraBuf[i].read_id];
            mCameraBuf[i].read_id++;
            mCameraBuf[i].buf_used--;
            if (mCameraBuf[i].read_id >= NB_COMPOSE_BUFFER)
            {
                mCameraBuf[i].read_id = 0;
            }
            pthread_mutex_unlock(&mComposeMutex);
            ALOGV("too many buffers so release camrea[%d].index=%d refcnt=%d", i + mStartCameraID, buffer->index, buffer->refCnt);
            mCameraHardware[i+mStartCameraID]->releasePreviewFrame(buffer->index);
            pthread_mutex_lock(&mComposeMutex);
        }
    }
    pthread_mutex_unlock(&mComposeMutex);
    F_LOGD;
    return canCompose;
}

void CameraManager::releaseAllCameraBuff()
{
    int i;
    bool canCompose = true;
    pthread_mutex_lock(&mComposeMutex);
    for(i=0;i<mCameraTotalNum;i++)
    {
        while(mCameraBuf[i].buf_used > 0)
        {
            V4L2BUF_t *buffer;// = (mCameraBuf[i].buf[mCameraBuf[i].read_id]);
            //memcpy(&buffer,&mCameraBuf[i].buf[mCameraBuf[i].read_id],sizeof(V4L2BUF_t));
            buffer = mCameraBuf[i].buf[mCameraBuf[i].read_id];
            mCameraBuf[i].read_id++;
            mCameraBuf[i].buf_used--;
            if(mCameraBuf[i].read_id >= NB_COMPOSE_BUFFER)
            {
                mCameraBuf[i].read_id = 0;
            }
            pthread_mutex_unlock(&mComposeMutex);
            mCameraHardware[i+mStartCameraID]->releasePreviewFrame(buffer->index);
            pthread_mutex_lock(&mComposeMutex);
            ALOGV("FUNC:%s, CamID=%d, mCameraBuf[i].buf_used =%d", __FUNCTION__, i, mCameraBuf[i].buf_used);
        }
    }
    pthread_mutex_unlock(&mComposeMutex);
    F_LOGD;
}

void CameraManager::releaseByIndex(int index)
{
    if (isOviewEnable())
    {
        for (int i = 0; i < mCameraTotalNum; i++)
        {
            mCameraHardware[mStartCameraID + i]->mV4L2CameraDevice->releasePreviewFrame(mReleaseIndex[index][i]);
            ALOGV("[camera %d].release buffer index=%d", i + mStartCameraID, mReleaseIndex[index][i]);
        }
    }
}

int CameraManager::setFrameSize(int index,int width,int height)
{
    pthread_mutex_lock(&mComposeMutex);
    mFrameWidth = width;
    mFrameHeight = height;
    LOGD("V4L2-Debug,F:%s,L:%d,mFrameWidth:%d,mFrameHeight:%d,id:%d",__FUNCTION__,__LINE__,mFrameWidth,mFrameHeight,index);
    pthread_mutex_unlock(&mComposeMutex);
    F_LOGD;
    return 0;
}

bool CameraManager::isSameSize()
{
    int width;
    int height;
    int i;

    width = mCameraHardware[mStartCameraID]->getFrameWidth();
    height = mCameraHardware[mStartCameraID]->getFrameHeight();
    for(i=1;i<mCameraTotalNum;i++)
    {
        if((width != mCameraHardware[i+ mStartCameraID]->getFrameWidth())
            ||(height != mCameraHardware[i+mStartCameraID]->getFrameHeight()))
            return false;
    }
    F_LOGD;

    return true;
}

int CameraManager::queueCameraBuf(int index,V4L2BUF_t *buffer)
{
    int i;

    pthread_mutex_lock(&mComposeMutex);

    if ((mCaptureState == CAPTURE_STATE_EXIT) || (mCaptureState == CAPTURE_STATE_PAUSE))
    {
        pthread_mutex_unlock(&mComposeMutex);
        ALOGE("Camera[%d], invalid capture state:%d, F:%s,L:%d", index, mCaptureState, __FUNCTION__, __LINE__);
        return -1;
    }

    ALOGV("Camera[%d], Capture state:%d, F:%s,L:%d", index, mCaptureState, __FUNCTION__, __LINE__);

    if ((index < mStartCameraID) || (index >= mStartCameraID + mCameraTotalNum))
    {
        pthread_mutex_unlock(&mComposeMutex);
        ALOGE("Camera[%d] out of range [%d,%d)", index, mStartCameraID, mStartCameraID + mCameraTotalNum);
        return -1;
    }

    index = index - mStartCameraID;

    if (mCaptureState == CAPTURE_STATE_READY)
    {
        mCameraState[index] = true;
        for (i = 0; i < mCameraTotalNum; i++)
        {
            if (mCameraState[i] == false)
            {
                pthread_mutex_unlock(&mComposeMutex);
                ALOGE("Camera[%d]isPrepare ,but Camera[%d] is not ready yet!",index+mStartCameraID,i + mStartCameraID);
                return -1;
            }
        }

        if (!isSameSize())
        {
            mCaptureState = CAPTURE_STATE_PAUSE;
            pthread_mutex_unlock(&mComposeMutex);
            ALOGE("Camera size is not the same,So it's can not be composed");
            return -1;
        }

        ALOGD("OK,It's will do camera buffer compose");
        mCaptureState = CAPTURE_STATE_STARTED;
    }

    if (mCameraBuf[index].buf_used < NB_COMPOSE_BUFFER)
    {
        buffer->refMutex.lock();
        buffer->refCnt++;
        buffer->refMutex.unlock();
        ALOGV("Camera[%d], buffer index:%d, refcnt:%d", index + mStartCameraID, buffer->index, buffer->refCnt);
        //memcpy(&mCameraBuf[index].buf[mCameraBuf[index].write_id], buffer, sizeof(V4L2BUF_t));
        mCameraBuf[index].buf[mCameraBuf[index].write_id] = buffer;
        mCameraBuf[index].write_id++;
        if(mCameraBuf[index].write_id >= NB_COMPOSE_BUFFER)
        {
            mCameraBuf[index].write_id = 0;
        }
        mCameraBuf[index].buf_used++;
        ALOGV("F:%s,L:%d,Camera[%d],buf_used:%d,buf_index=%d",__FUNCTION__,__LINE__,index + mStartCameraID,mCameraBuf[index].buf_used,buffer->index);
    }
    else
    {
        ALOGE("Camera[%d].buf_used >= NB_COMPOSE_BUFFER",index + mStartCameraID);
    }

    pthread_cond_signal(&mComposeCond);
    pthread_mutex_unlock(&mComposeMutex);
    F_LOGD;
    return 0;
}

bool CameraManager::composeThread()
{
    int i;
    V4L2BUF_t *writeBuffer = NULL;
    V4L2BUF_t *inbuffer[mCameraTotalNum];
    if (mCaptureState == CAPTURE_STATE_EXIT)
    {
        ALOGV("Capture state:%d, sleep in F:%s,L:%d", mCaptureState, __FUNCTION__, __LINE__);
        releaseAllCameraBuff();
        pthread_mutex_lock(&mComposeMutex);
        pthread_cond_wait(&mComposeCond, &mComposeMutex);
        pthread_mutex_unlock(&mComposeMutex);
        return true;
    }
    if (mCaptureState == CAPTURE_STATE_PAUSE)
    {
        ALOGV("Capture state:%d, sleep in F:%s,L:%d", mCaptureState, __FUNCTION__, __LINE__);
        pthread_mutex_lock(&mComposeMutex);
        pthread_cond_wait(&mComposeCond, &mComposeMutex);
        pthread_mutex_unlock(&mComposeMutex);
        return true;
    }
    bool bufReady = canCompose();
    if (!bufReady)
    {
        ALOGV("Has no enough camera buffer, sleep in F:%s,L:%d", __FUNCTION__, __LINE__);
        pthread_mutex_lock(&mComposeMutex);
        struct timespec timeout;
        timeout.tv_sec=time(0);
        timeout.tv_nsec = 100000000; // 100ms timeout
        int ret = pthread_cond_timedwait(&mComposeCond, &mComposeMutex, &timeout);
        pthread_mutex_unlock(&mComposeMutex);
        return true;
    }
    pthread_mutex_lock(&mComposeMutex);
    for (i = 0; i < mCameraTotalNum; i++)
    {
        //memcpy(&inbuffer[i], &mCameraBuf[i].buf[mCameraBuf[i].read_id], sizeof(V4L2BUF_t));
        inbuffer[i] = mCameraBuf[i].buf[mCameraBuf[i].read_id];
        mCameraBuf[i].read_id++;
        mCameraBuf[i].buf_used--;
        if (mCameraBuf[i].read_id >= NB_COMPOSE_BUFFER)
        {
            mCameraBuf[i].read_id = 0;
        }
    }
    pthread_mutex_unlock(&mComposeMutex);
    V4L2BUF_t *tmpBuf[mCameraTotalNum];
    int sIndex = inbuffer[0]->index;

    for (i = 0; i < mCameraTotalNum; i++)
    {
        mReleaseIndex[sIndex][i] = inbuffer[i]->index;
        ALOGV("[camera %d].need release index=%d", i + mStartCameraID, inbuffer[i]->index);
    }
    mCameraHardware[mStartCameraID]->mCallbackNotifier.onNextFrameAvailable((const void **)(&inbuffer), mCameraTotalNum);

    for (i = 0; i < mCameraTotalNum; i++)
    {
        if (inbuffer[i]->addrVirY != 0)
        {
            ALOGV("release [camera %d].index=%d", i + mStartCameraID, inbuffer[i]->index);
            mCameraHardware[i + mStartCameraID]->releasePreviewFrame(inbuffer[i]->index);
        }
    }
    return true;
}

bool CameraManager::commandThread()
{
    int i;
    pthread_mutex_lock(&mCommandMutex);
    Queue_CM_Element * queue = (Queue_CM_Element *)OSAL_Dequeue(&mQueueCMCommand);
    if (queue == NULL)
    {
        ALOGV("Sleep in F:%s,L:%d", __FUNCTION__, __LINE__);
        pthread_cond_wait(&mCommandCond, &mCommandMutex);
        pthread_mutex_unlock(&mCommandMutex);
        return true;
    }
    pthread_mutex_unlock(&mCommandMutex);
    switch(queue->cmd)
    {
        case CMD_CM_START_PREVIEW:
        {
            LOGD("CameraManDubug,F:%s,L:%d",__FUNCTION__,__LINE__);
            for(i= mStartCameraID+ 1;i<mCameraTotalNum + mStartCameraID;i++)
            {
                ALOGD("commandThread mCameraHardware[%d]->startPreview_h(),mCameraHardware[%d]=0x%x",i,i,mCameraHardware[i]);
                LOGD("CameraManDubug,F:%s,L:%d",__FUNCTION__,__LINE__);
                mCameraHardware[i]->startPreview_h();
                ALOGD("commandThread mCameraHardware[%d]->startPreview_h() end",i);
            }
            LOGD("CameraManDubug,F:%s,L:%d",__FUNCTION__,__LINE__);

            pthread_mutex_lock(&mComposeMutex);
            for(i=0;i<mCameraTotalNum;i++)
            {
                mCameraBuf[i].buf_used = 0;
            }

            pthread_mutex_unlock(&mComposeMutex);
            F_LOGD;
            break;
        }
        case CMD_CM_STOP_PREVIEW:
        {
            int i = 0;
            for(i= mStartCameraID+ 1;i<mCameraTotalNum + mStartCameraID;i++)
            {
                mCameraHardware[i]->stopPreview_h();
            }
            F_LOGD;
            break;
        }
        case CMD_CM_RELEASE_CAMERA:
        {
            int i = 0;
            for(i= mStartCameraID+ 1;i<mCameraTotalNum + mStartCameraID;i++)
            {
                F_LOG;
                mCameraHardware[i]->releaseCamera_h();
                F_LOG;
            }
            F_LOGD;
            break;
        }
        default:
            ALOGW("unknown queue command: %d", queue->cmd);
            break;
    }
    return true;
}

void CameraManager::startPreview()
{
#if SAVE_FRAME
    count = 0;
#endif
    memset(&mCameraBuf,0x0,sizeof(BufManager)*MAX_NUM_OF_CAMERAS);
    memset(mCameraState,0,sizeof(bool)*MAX_NUM_OF_CAMERAS);
    memset(mReleaseIndex, 0, sizeof(mReleaseIndex));
    mAbandonFrameCnt = ABANDONFRAMECT;
    pthread_mutex_lock(&mCommandMutex);
    mQueueCMElement[CMD_CM_START_PREVIEW].cmd = CMD_CM_START_PREVIEW;
    OSAL_Queue(&mQueueCMCommand, &mQueueCMElement[CMD_CM_START_PREVIEW]);
    pthread_cond_signal(&mCommandCond);
    pthread_mutex_unlock(&mCommandMutex);
    pthread_mutex_lock(&mComposeMutex);
    mCaptureState = CAPTURE_STATE_READY;
    pthread_mutex_unlock(&mComposeMutex);
    LOGD("CameraManDubug,F:%s,L:%d",__FUNCTION__,__LINE__);
    F_LOGD;
}

void CameraManager::stopPreview()
{
    pthread_mutex_lock(&mComposeMutex);
    mCaptureState = CAPTURE_STATE_PAUSE;
    pthread_mutex_unlock(&mComposeMutex);
    pthread_mutex_lock(&mCommandMutex);
    mQueueCMElement[CMD_CM_STOP_PREVIEW].cmd = CMD_CM_STOP_PREVIEW;
    OSAL_Queue(&mQueueCMCommand, &mQueueCMElement[CMD_CM_STOP_PREVIEW]);
    pthread_cond_signal(&mCommandCond);
    pthread_mutex_unlock(&mCommandMutex);
    F_LOGD;
}

void CameraManager::releaseCamera()
{
    F_LOGD;
    pthread_mutex_lock(&mComposeMutex);
    mCaptureState = CAPTURE_STATE_EXIT;
    pthread_mutex_unlock(&mComposeMutex);
    pthread_mutex_lock(&mCommandMutex);
    mQueueCMElement[CMD_CM_RELEASE_CAMERA].cmd = CMD_CM_RELEASE_CAMERA;
    OSAL_Queue(&mQueueCMCommand, &mQueueCMElement[CMD_CM_RELEASE_CAMERA]);
    pthread_cond_signal(&mCommandCond);
    pthread_mutex_unlock(&mCommandMutex);
    //composeBufDeinit();
    if(isOviewEnable())
        setOviewEnable(false);
}

status_t CameraManager::setCameraHardware(int index,CameraHardware *hardware)
{
    if(index < mStartCameraID)
        return EINVAL;
    if (hardware == NULL)
    {
        LOGE("ERROR hardware info");
        return EINVAL;
    }
    mCameraHardware[index] = hardware;
    return NO_ERROR;
}

CameraManager::~CameraManager()
{
    ALOGV("~CameraManager");
    if (mComposeThread != NULL)
    {
        mComposeThread->stopThread();
        mComposeThread.clear();
        mComposeThread = 0;
    }
    if (mCommandThread != NULL)
    {
        mCommandThread->stopThread();
        pthread_cond_signal(&mCommandCond);
        mCommandThread.clear();
        mCommandThread = 0;
    }

    ALOGD("%s-%d",__FUNCTION__,__LINE__);
    pthread_cond_destroy(&mCommandCond);
    pthread_mutex_destroy(&mCommandMutex);
    ALOGD("%s-%d",__FUNCTION__,__LINE__);
    ALOGD("%s-%d",__FUNCTION__,__LINE__);
    pthread_cond_destroy(&mComposeCond);
    pthread_mutex_destroy(&mComposeMutex);
    ALOGD("%s-%d",__FUNCTION__,__LINE__);
    OSAL_QueueTerminate(&mQueueCMCommand);
}
}
#endif //CAMERA_MANAGER_ENABLE
