/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "CompositionEngineV2Impl.h"
#include "CompositionEngineV2Impl_defs.h"
#include "Debug.h"
#include "disputils.h"
#include "helper.h"
#include "LayerPlanner.h"
#include "errorcode.h"

namespace sunxi {

// static
std::shared_ptr<CompositionEngineV2Impl> CompositionEngineV2Impl::create(int id)
{
    if (id < 0 || id >= CONFIG_MAX_DISPLAY_ENGINE) {
        DLOGE("Display Engine id(%d) not support");
        return nullptr;
    }
    std::shared_ptr<CompositionEngineV2Impl> engine(new CompositionEngineV2Impl(id));
    if (engine->setup() != 0) {
        DLOGE("Display Engine init error");
        return nullptr;
    }
    return engine;
}

CompositionEngineV2Impl::CompositionEngineV2Impl(int id)
  : mHardwareIndex(id),
    mSyncTimelineActive(false),
    mAfbcBufferSupported(false),
    mIommuEnabled(true)
{ }

CompositionEngineV2Impl::~CompositionEngineV2Impl()
{
    if (mSyncTimelineActive) {
        int error = destroySyncTimeline(mHardwareIndex);
        if (error != 0)
            DLOGE("destroySyncTimeline return error = %d", error);
    }
    DLOGI("CompositionEngine(%d) destroy", mHardwareIndex);
}

int CompositionEngineV2Impl::setup()
{
    int error = createSyncTimeline(mHardwareIndex);
    if (error != 0) {
        DLOGE("createSyncTimeline return error = %d", error);
        return error;
    }
    mSyncTimelineActive = true;

    const auto& info = HardwareConfigs[mHardwareIndex];
    mAttribute.featureMask = info.featureMask;
    mAttribute.maxHybridChannelCount = info.videoChannelCount;
    mAttribute.maxRGBChannelCount = info.uiChannelCount;

    // Choose a suitable bandwidth config
    int dramFreq = getDramFrequency();
    if (dramFreq <= 0) {
        DLOGE("getDramFrequency return error(%d), fallback to default bandwidth config", dramFreq);
        mAttribute.maxBandwidth =
            BandwidthConfigs[CONFIG_MAX_BANDWIDTH_LEVE - 1].maxAvaliableBandwidth;
    } else {
        int bandwidth = 0;
        for (int i = 0; i < CONFIG_MAX_BANDWIDTH_LEVE; i++) {
            if (dramFreq >= BandwidthConfigs[i].dramFrequency) {
                bandwidth = BandwidthConfigs[i].maxAvaliableBandwidth;
                break;
            }
        }
        mAttribute.maxBandwidth = bandwidth ?
            bandwidth : BandwidthConfigs[CONFIG_MAX_BANDWIDTH_LEVE - 1].maxAvaliableBandwidth;
    }
    mFrequency = ::getDramFrequency();
    return 0;
}

const CompositionEngine::Attribute& CompositionEngineV2Impl::getAttribute() const
{
    return mAttribute;
}

void CompositionEngineV2Impl::rearrangeChannel(
        std::vector<std::shared_ptr<ChannelConfig>>& configs) const
{
    std::vector<std::shared_ptr<ChannelConfig>> vchannels;
    for (auto iter = configs.begin(); iter != configs.end();) {
        if ((*iter)->VideoChannel == true) {
            vchannels.push_back(*iter);
            iter = configs.erase(iter);
        } else {
            ++iter;
        }
    }
    configs.insert(configs.begin(), vchannels.begin(), vchannels.end());

    size_t hardwareIndex = 0;
    std::for_each(configs.begin(), configs.end(),
            [&hardwareIndex](const auto& config) {
            config->Index = hardwareIndex;
            hardwareIndex++;
            });
}

int CompositionEngineV2Impl::createSyncpt(syncinfo *info)
{
    if (info == nullptr) {
        DLOGE("invalid argument, info is nullptr");
        return -1;
    }
    return ::createSyncpt(mHardwareIndex, info);
}

int CompositionEngineV2Impl::submitLayer(
        unsigned int syncnum, disp_layer_config2* configs, int configCount)
{
    DTRACE_FUNC();
    if (configs == nullptr) {
        DLOGE("invalid argument, configs is nullptr");
        return -1;
    }
    return ::submitLayer(mHardwareIndex, syncnum, configs, configCount);
}

int CompositionEngineV2Impl::capable(
        const ScreenTransform& transform, const std::shared_ptr<Layer>& layer)
{
    if (!layer || !layer->bufferHandle()) {
        DLOGW("nullptr layer or nullptr buffer");
        return eNullBufferHandle;
    }

    // buffer fromat filter
    if (!compositionEngineV2FormatFilter(getLayerPixelFormat(layer))) {
        return eNotSupportFormat;
    }

    // scaler check
    if (!hardwareScalerCheck(mFrequency, transform,
                isYuvFormat(layer), layer->sourceCrop(), layer->displayFrame())) {
        return eScaleError;
    }

    if (isAfbcBuffer(layer->bufferHandle())) {
        if (mAfbcBufferSupported) {
            bool supported = afbcBufferScaleCheck(mFrequency,
                    transform, layer->sourceCrop(), layer->displayFrame());
            if (!supported) {
                return eScaleError;
            }
        } else {
            return eAfbcBuffer;
        }
    }

    if (!mIommuEnabled
            && !isPhysicalContinuousBuffer(layer->bufferHandle())) {
        return eNonPhysicalContinuousMemory;
    }

    return 0;
}

} // namespace sunxi
