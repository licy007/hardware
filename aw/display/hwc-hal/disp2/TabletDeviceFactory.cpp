/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Debug.h"
#include "disputils.h"
#include "uniquefd.h"
#include "CompositionEngineV2Impl.h"
#include "TabletDeviceFactory.h"
#include "hardware/sunxi_display2.h"
#include "RotatorManager.h"
#include "vendorservice/default/VendorServiceAdapter.h"

namespace sunxi {

TabletDeviceFactory::TabletDeviceFactory()
    : mConnectedDisplays(),
      mListener(nullptr)
{
    VendorServiceAdapter::getService();
}

TabletDeviceFactory::~TabletDeviceFactory()
{
    mConnectedDisplays.clear();
    mListener = nullptr;
}

void TabletDeviceFactory::scanConnectedDevice()
{
    std::lock_guard<std::mutex> lock(mDeviceMutex);
    mConnectedDisplays.clear();

    for (int hardwareIndex  = 0; hardwareIndex < MaxDisplayCount; hardwareIndex++) {
        int type = getDisplayOutputType(hardwareIndex);
        const char *typestr = outputType2Name(type);
        DLOGI("DE[%d] output type: %s(%d)", hardwareIndex, typestr, type);

        if (type == DISP_OUTPUT_TYPE_LCD) {
            DeviceBase::Config config = makeLcdDefaultConfig();
            std::string name = std::string(typestr) + std::to_string(hardwareIndex);
            auto engine = CompositionEngineV2Impl::create(hardwareIndex);
            auto device = std::make_shared<Disp2Device>(name, engine, config);
            device->setOutput(config.width, config.height, config.refreshRate);

            std::shared_ptr<RotatorManager> rotator = std::make_shared<RotatorManager>();
            device->setRotatorManager(rotator);

            ConnectedInfo c;
            c.displayId = HWC_DISPLAY_PRIMARY;  // always assume lcd as primary display
            c.connected = 1;
            c.device = device;
            mConnectedDisplays.push_back(c);
            break;
        } else {
            DLOGW("not support type [%s] on tablet platform yet!", typestr);
        }
    }

    if (!mListener) {
        DLOGE("DeviceChangedListener not register yet!");
        return;
    }
    mListener->onDeviceChanged(mConnectedDisplays);
}

DeviceBase::Config TabletDeviceFactory::makeLcdDefaultConfig() const
{
    DeviceBase::Config config = {
        .name = std::string("default-config"),
        .configId = 0,
        .width = 1280,
        .height = 720,
        .dpix = 160,
        .dpiy = 160,
        .refreshRate = 60,
    };
    struct fb_var_screeninfo info;
    if (getFramebufferVarScreenInfo(&info) != 0) {
        DLOGE("get var screeninfo failed!");
        return config;
    }

    int refreshRate = 1000000000000LLU /
        (uint64_t(info.upper_margin + info.lower_margin + info.vsync_len + info.yres)
         * ( info.left_margin  + info.right_margin + info.hsync_len + info.xres)
         * info.pixclock);
    if (refreshRate == 0) {
        DLOGE("invalid refresh rate, assuming 60 Hz");
        refreshRate = 60;
    }
    config.refreshRate = refreshRate;

    if (info.width != 0 && info.height != 0) {
        config.dpix = 1000 * (info.xres * 25.4f) / info.width;
        config.dpiy = 1000 * (info.yres * 25.4f) / info.height;
    }
    config.width  = info.xres;
    config.height = info.yres;

    return config;
}

void TabletDeviceFactory::registerDeviceChangedListener(DeviceChangedListener* listener)
{
    std::lock_guard<std::mutex> lock(mDeviceMutex);
    mListener = listener;
}

std::shared_ptr<DeviceBase> TabletDeviceFactory::getPrimaryDevice()
{
    std::lock_guard<std::mutex> lock(mDeviceMutex);
    std::shared_ptr<DeviceBase> device = nullptr;
    for (auto& item : mConnectedDisplays) {
        if (item.displayId == HWC_DISPLAY_PRIMARY) {
            device = item.device;
            break;
        }
    }
    return device;
}

// require by HWComposer.cpp
std::unique_ptr<IDeviceFactory> createDeviceFactory() {
    DLOGI("IDeviceFactory from %s", __FILE__);
    return std::make_unique<TabletDeviceFactory>();
}

} // namespace sunxi

