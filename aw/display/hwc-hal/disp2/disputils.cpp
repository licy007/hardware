

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

#include "Debug.h"
#include "disputils.h"
#include "uniquefd.h"

// Wrapper class to maintain dispdev fd
class dispdev {
public:
    static dispdev& getInstance() {
        static dispdev _instance;
        return _instance;
    }
    inline int getHandleFd() { return mDispfd.get(); }
    static int fd() { return getInstance().getHandleFd(); }

private:
    dispdev();
   ~dispdev() = default;
    dispdev(const dispdev&) = delete;
    dispdev& operator=(const dispdev&) = delete;
    sunxi::uniquefd mDispfd;
};

dispdev::dispdev() {
    int fd = open("/dev/disp", O_RDWR);
    if (fd < 0) {
        DLOGE("Open '/dev/disp' failed, %s", strerror(errno));
    }
    mDispfd = sunxi::uniquefd(fd);
}

// default DE frequency 254 MHz
static int __deFrequency = 254000000;


// dev_composer defines -->
enum {
    // create a fence timeline
    HWC_NEW_CLIENT = 1,
    HWC_DESTROY_CLIENT,
    HWC_ACQUIRE_FENCE,
    HWC_SUBMIT_FENCE,
};
// dev_composer defines <--

// disp sync opt
int createSyncTimeline(int disp)
{
    unsigned long deFreq;
    unsigned long args[4] = {0};

    args[0] = disp;
    args[1] = HWC_NEW_CLIENT;
    args[2] = (unsigned long)&deFreq;

    if (ioctl(dispdev::fd(), DISP_HWC_COMMIT, (unsigned long)args)) {
        DLOGE("createSyncTimeline failed, %s", strerror(errno));
        return -1;
    }
    __deFrequency = deFreq;
    return 0;
}

int createSyncpt(int disp, syncinfo *info)
{
    unsigned long args[4] = {0};

    args[0] = disp;
    args[1] = HWC_ACQUIRE_FENCE;
    args[2] = (unsigned long)info;
    args[3] = 0;

    if (ioctl(dispdev::fd(), DISP_HWC_COMMIT, (unsigned long)args)) {
        DLOGE("createSyncpt failed!");
        return -ENODEV;
    }
    return 0;
}

int destroySyncTimeline(int disp)
{
    unsigned long args[4] = {0};

    args[0] = disp;
    args[1] = HWC_DESTROY_CLIENT;

    if (ioctl(dispdev::fd(), DISP_HWC_COMMIT, (unsigned long)args)) {
        DLOGE("destroySyncTimeline failed!");
        return -1;
    }
    return 0;
}

// disp device opt
int submitLayer(int disp, unsigned int syncnum,
        disp_layer_config2 *configs, int configCount)
{
    unsigned long args[4] = {0};

    args[0] = disp;
    args[1] = 1;
    if (ioctl(dispdev::fd(), DISP_SHADOW_PROTECT, (unsigned long)args)) {
        DLOGE("ioctl DISP_SHADOW_PROTECT error");
        return -1;
    }

    args[0] = disp;
    args[1] = (unsigned long)(configs);
    args[2] = configCount;
    args[3] = 0;

    if (ioctl(dispdev::fd(), DISP_LAYER_SET_CONFIG2, (unsigned long)args)) {
        DLOGE("ioctl DISP_LAYER_SET_CONFIG2 error");
    }

    args[0] = disp;
    args[1] = HWC_SUBMIT_FENCE;
    args[2] = syncnum;
    args[3] = 0;
    if (ioctl(dispdev::fd(), DISP_HWC_COMMIT, (unsigned long)args)) {
        DLOGE("ioctl DISP_HWC_COMMIT error");
    }

    args[0] = disp;
    args[1] = 0;
    args[2] = 0;
    args[3] = 0;
    if (ioctl(dispdev::fd(), DISP_SHADOW_PROTECT, (unsigned long)args)) {
        DLOGE("ioctl DISP_SHADOW_PROTECT error");
        return -1;
    }
    return 0;
}

int vsyncCtrl(int disp, int enable)
{
    unsigned long args[4] = {0};
    args[0] = disp;
    args[1] = enable;
    if (ioctl(dispdev::fd(), DISP_VSYNC_EVENT_EN, (unsigned long)args)) {
        DLOGE("ioctl DISP_VSYNC_EVENT_EN error");
        return -1;
    }
    return 0;
}

int getDeFrequency()
{
    return __deFrequency;
}

int getDramFrequency()
{
    // 572 MHz
    return 572000000;
}

int getFramebufferVarScreenInfo(struct fb_var_screeninfo* info)
{
    int fd = open("/dev/graphics/fb0", O_RDWR);
    if (fd < 0) {
        DLOGE("Open '/dev/graphics/fb0' failed, %s", strerror(errno));
        return -1;
    }
    int ret = ioctl(fd, FBIOGET_VSCREENINFO, info);
    close(fd);
    return ret;
}

int getDisplayOutputType(int disp)
{
    struct disp_output info;
    unsigned long args[4] = {0};
    args[0] = disp;;
    args[1] = (unsigned long)&info;
    if (ioctl(dispdev::fd(), DISP_GET_OUTPUT, (unsigned long)args) == 0) {
        return info.type;
    }
    DLOGE("get display output info failed, display=%d", disp);
    return -1;
}

const char *outputType2Name(int type)
{
    switch (type) {
        case DISP_OUTPUT_TYPE_HDMI: return "hdmi";
        case DISP_OUTPUT_TYPE_TV:   return "cvbs";
        case DISP_OUTPUT_TYPE_LCD:  return "lcd";
        case DISP_OUTPUT_TYPE_VGA:  return "vga";
        default:                    return "unknow";
    }
}

