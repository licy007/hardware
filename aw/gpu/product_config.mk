#
# Copyright (C) 2017-2019 Allwinner Technology Co., Ltd. All rights reserved.
#


COMMON_MK_FILE := $(word $(shell expr $(words $(MAKEFILE_LIST)) - 1), $(MAKEFILE_LIST))
BOARD_CONFIG_FILE := $(shell dirname $(COMMON_MK_FILE))/BoardConfigCommon.mk
TARGET_GPU_TYPE := $(filter-out  , $(shell grep TARGET_GPU_TYPE $(BOARD_CONFIG_FILE) | head -n 1 | cut -d = -f 2))
TARGET_ARCH := $(filter-out  ,$(shell grep TARGET_ARCH $(BOARD_CONFIG_FILE) | head -n 1 | cut -d = -f 2))
TARGET_BOARD_PLATFORM := $(filter-out  ,$(shell grep TARGET_BOARD_PLATFORM $(BOARD_CONFIG_FILE) | head -n 1 | cut -d = -f 2))

GPU_ROOT := $(shell dirname $(lastword $(MAKEFILE_LIST)))

ifneq ($(filter $(TARGET_GPU_TYPE),mali400 mali450),)
GPU_ARCH := mali-utgard
else ifneq ($(filter $(TARGET_GPU_TYPE),mali-t720 mali-t760),)
GPU_ARCH := mali-midgard
else ifneq ($(filter $(TARGET_GPU_TYPE),sgx544),)
GPU_ARCH := img-sgx
else ifneq ($(filter $(TARGET_GPU_TYPE),mali-g31),)
GPU_ARCH := mali-bifrost
else ifneq ($(filter $(TARGET_GPU_TYPE),ge8300),)
GPU_ARCH := img-rgx
else
$(error TARGET_GPU_TYPE($(TARGET_GPU_TYPE)) is invalid!)
endif

# Get OpenGL ES and Vulkan version
GLES_VERSION :=
VULKAN_VERSION :=
ifneq ($(filter $(TARGET_GPU_TYPE),mali400 mali450 sgx544),)
GLES_VERSION := 2.0
endif
ifeq ($(TARGET_GPU_TYPE),mali-t720)
GLES_VERSION := 3.1
SUPPORT_AEP := 1
endif
ifeq ($(TARGET_GPU_TYPE),mali-t760)
GLES_VERSION := 3.2
SUPPORT_AEP := 1
endif
ifeq ($(TARGET_GPU_TYPE),mali-g31)
GLES_VERSION := 3.2
SUPPORT_AEP := 1
VULKAN_VERSION :=
endif
ifeq ($(TARGET_GPU_TYPE),ge8300)
GLES_VERSION := 3.2
SUPPORT_AEP := 1
VULKAN_VERSION := 1.1
endif

GPU_COPY_ROOT_DIR := $(GPU_ROOT)/$(GPU_ARCH)/$(TARGET_GPU_TYPE)/$(TARGET_ARCH)

ifeq ($(wildcard $(GPU_COPY_ROOT_DIR)/lib/*.so),)
$(error There is no libraries in $(GPU_COPY_ROOT_DIR)!)
endif

ifneq ($(wildcard $(GPU_ROOT)/$(GPU_ARCH)/egl.cfg),)
PRODUCT_COPY_FILES += $(GPU_ROOT)/$(GPU_ARCH)/egl.cfg:system/lib/egl/egl.cfg
endif

# For Mali GPUs
ifneq ($(filter mali-%, $(GPU_ARCH)),)

PRODUCT_PACKAGES += \
	gralloc.$(TARGET_BOARD_PLATFORM)

PRODUCT_COPY_FILES += \
	$(GPU_COPY_ROOT_DIR)/lib/libGLES_mali.so:vendor/lib/egl/libGLES_mali.so
ifeq ($(TARGET_ARCH),arm64)
PRODUCT_COPY_FILES += \
	$(GPU_COPY_ROOT_DIR)/lib64/libGLES_mali.so:vendor/lib64/egl/libGLES_mali.so
endif

endif

# For IMG GPUs
ifneq ($(filter img-%, $(GPU_ARCH)),)
ifneq ($(wildcard $(GPU_COPY_ROOT_DIR)/powervr.ini),)
PRODUCT_COPY_FILES += $(GPU_COPY_ROOT_DIR)/powervr.ini:etc/powervr.ini
endif
endif

ifeq ($(GPU_ARCH),img-sgx)
PRODUCT_COPY_FILES += \
	$(GPU_COPY_ROOT_DIR)/lib/libusc.so:vendor/lib/libusc.so \
	$(GPU_COPY_ROOT_DIR)/lib/libglslcompiler.so:vendor/lib/libglslcompiler.so \
	$(GPU_COPY_ROOT_DIR)/lib/libIMGegl.so:vendor/lib/libIMGegl.so \
	$(GPU_COPY_ROOT_DIR)/lib/libpvr2d.so:vendor/lib/libpvr2d.so \
	$(GPU_COPY_ROOT_DIR)/lib/libpvrANDROID_WSEGL.so:vendor/lib/libpvrANDROID_WSEGL.so \
	$(GPU_COPY_ROOT_DIR)/lib/libPVRScopeServices.so:vendor/lib/libPVRScopeServices.so \
	$(GPU_COPY_ROOT_DIR)/lib/libsrv_init.so:vendor/lib/libsrv_init.so \
	$(GPU_COPY_ROOT_DIR)/lib/libsrv_um.so:vendor/lib/libsrv_um.so \
	$(GPU_COPY_ROOT_DIR)/lib/libEGL_POWERVR_SGX544_115.so:vendor/lib/egl/libEGL_POWERVR_SGX544_115.so \
	$(GPU_COPY_ROOT_DIR)/lib/libGLESv1_CM_POWERVR_SGX544_115.so:vendor/lib/egl/libGLESv1_CM_POWERVR_SGX544_115.so \
	$(GPU_COPY_ROOT_DIR)/lib/libGLESv2_POWERVR_SGX544_115.so:vendor/lib/egl/libGLESv2_POWERVR_SGX544_115.so \
	$(GPU_COPY_ROOT_DIR)/lib/gralloc.sunxi.so:vendor/lib/hw/gralloc.$(TARGET_BOARD_PLATFORM).so \
	$(GPU_COPY_ROOT_DIR)/bin/pvrsrvctl:vendor/bin/pvrsrvctl
endif

ifeq ($(GPU_ARCH),img-rgx)
PRODUCT_COPY_FILES += \
	$(GPU_COPY_ROOT_DIR)/lib/libusc.so:vendor/lib/libusc.so \
	$(GPU_COPY_ROOT_DIR)/lib/libglslcompiler.so:vendor/lib/libglslcompiler.so \
	$(GPU_COPY_ROOT_DIR)/lib/libIMGegl.so:vendor/lib/libIMGegl.so \
	$(GPU_COPY_ROOT_DIR)/lib/libPVRScopeServices.so:vendor/lib/libPVRScopeServices.so \
	$(GPU_COPY_ROOT_DIR)/lib/libsrv_um.so:vendor/lib/libsrv_um.so \
	$(GPU_COPY_ROOT_DIR)/lib/libEGL_POWERVR_ROGUE.so:vendor/lib/egl/libEGL_POWERVR_ROGUE.so \
	$(GPU_COPY_ROOT_DIR)/lib/libGLESv1_CM_POWERVR_ROGUE.so:vendor/lib/egl/libGLESv1_CM_POWERVR_ROGUE.so \
	$(GPU_COPY_ROOT_DIR)/lib/libGLESv2_POWERVR_ROGUE.so:vendor/lib/egl/libGLESv2_POWERVR_ROGUE.so \
	$(GPU_COPY_ROOT_DIR)/lib/libufwriter.so:vendor/lib/libufwriter.so \
	$(GPU_COPY_ROOT_DIR)/lib/gralloc.ceres.so:vendor/lib/hw/gralloc.$(TARGET_BOARD_PLATFORM).so \
	$(GPU_COPY_ROOT_DIR)/lib/vulkan.sunxi.so:vendor/lib/hw/vulkan.$(TARGET_BOARD_PLATFORM).so \
	$(GPU_COPY_ROOT_DIR)/firmware/rgx.fw.signed.22.102.54.38:vendor/firmware/rgx.fw.22.102.54.38
ifeq ($(TARGET_ARCH),arm64)
PRODUCT_COPY_FILES += \
	$(GPU_COPY_ROOT_DIR)/lib64/libusc.so:vendor/lib64/libusc.so \
	$(GPU_COPY_ROOT_DIR)/lib64/libglslcompiler.so:vendor/lib64/libglslcompiler.so \
	$(GPU_COPY_ROOT_DIR)/lib64/libIMGegl.so:vendor/lib64/libIMGegl.so \
	$(GPU_COPY_ROOT_DIR)/lib64/libPVRScopeServices.so:vendor/lib64/libPVRScopeServices.so \
	$(GPU_COPY_ROOT_DIR)/lib64/libsrv_um.so:vendor/lib64/libsrv_um.so \
	$(GPU_COPY_ROOT_DIR)/lib64/libEGL_POWERVR_ROGUE.so:vendor/lib64/egl/libEGL_POWERVR_ROGUE.so \
	$(GPU_COPY_ROOT_DIR)/lib64/libGLESv1_CM_POWERVR_ROGUE.so:vendor/lib64/egl/libGLESv1_CM_POWERVR_ROGUE.so \
	$(GPU_COPY_ROOT_DIR)/lib64/libGLESv2_POWERVR_ROGUE.so:vendor/lib64/egl/libGLESv2_POWERVR_ROGUE.so \
	$(GPU_COPY_ROOT_DIR)/lib64/gralloc.ceres.so:vendor/lib64/hw/gralloc.$(TARGET_BOARD_PLATFORM).so
endif
endif

# configure gpu opengles version:
# 131072=opengles 2.0
# 196608=opengles 3.0
# 196609=opengles 3.1
# 196610=opengles 3.2
ifeq ($(GLES_VERSION),2.0)
PRODUCT_PROPERTY_OVERRIDES += ro.opengles.version=131072
endif
ifeq ($(GLES_VERSION),3.0)
PRODUCT_PROPERTY_OVERRIDES += ro.opengles.version=196608
endif
ifeq ($(GLES_VERSION),3.1)
PRODUCT_PROPERTY_OVERRIDES += ro.opengles.version=196609
endif
ifeq ($(GLES_VERSION),3.2)
PRODUCT_PROPERTY_OVERRIDES += ro.opengles.version=196610
endif

# AEP (Android Extension Pack)
ifeq ($(SUPPORT_AEP),1)
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.opengles.aep.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.opengles.aep.xml
endif

ifeq ($(VULKAN_VERSION),1.1)
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.vulkan.level-1.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.level.xml \
	frameworks/native/data/etc/android.hardware.vulkan.compute-0.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.compute.xml \
	frameworks/native/data/etc/android.hardware.vulkan.version-1_1.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.version.xml
endif
